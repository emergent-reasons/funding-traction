import React, { PureComponent } from 'react';
import {
    Icon, 
    Row,
    Col,
    Card,
    CardTitle,
    ProgressBar
  } from 'react-materialize'

// import Grpc from './Grpc'
import Container from 'react-materialize/lib/Container';

class Home extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {  
            txs: [],
            server: localStorage.getItem('server'),
        };
    }

    render() {
        return (
            <div className="home-wrapper">
                {/* <Grpc /> */}
                <Container>
                    <Row>
                        <Col s={4} className="offset-s2">
                            <Card 
                                actions={[ <a key="1" href="/#">Project Details</a> ]}
                                closeIcon={<Icon>close</Icon>}
                                // header={<CardTitle image="https://eatbch.org/_assets/img/venezuela-hero-wide.jpg">EatBCH Venezuela</CardTitle>}
                                header={<CardTitle image="https://eatbch.org/_assets/img/venezuela-hero-wide.jpg">Lorem Ipsum</CardTitle>}
                                revealIcon={<Icon>more_vert</Icon>}
                            >
                                <Row>
                                    <Col s={12}>
                                        <span className="left">70%</span>
                                        <span className="right">70$ / 100$</span>
                                    </Col>
                                    <Col s={12}>
                                        <ProgressBar progress={70} />
                                    </Col>
                                    <Col s={12}>
                                        <span className="left"><Icon left>face</Icon> 12</span>
                                        <span className="right">Expires: 2020-02-15</span>
                                    </Col>
                                </Row>
                                
                                {/* We are Venezuelans doing what we can to help our neighbors in this time of uncertainty. Hyperinflation in Venezuela has created the most severe peacetime crisis the world has seen in decades. Thanks to your support, we've been able to bring food and smiles to hundreds of families living on the brink of starvation. */}
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non tincidunt quam. Cras maximus massa odio, non interdum nunc dictum non. Fusce mollis quis tortor a tincidunt. Donec quis tincidunt felis, condimentum interdum eros. Curabitur semper id felis condimentum ullamcorper. Cras id dolor pulvinar leo facilisis semper. 
                            </Card>
                        </Col>
                        <Col s={4}>
                            <Card 
                                actions={[ <a key="2" href="/#">Project Details</a> ]}
                                closeIcon={<Icon>close</Icon>}
                                // header={<CardTitle image="https://eatbch.org/_assets/img/south-sudan-hero-wide.jpg">EatBCH South Sudan</CardTitle>}
                                header={<CardTitle image="https://eatbch.org/_assets/img/south-sudan-hero-wide.jpg">Lorem Ipsum</CardTitle>}
                                revealIcon={<Icon>more_vert</Icon>}
                            >
                            <Row>
                                <Col s={12}>
                                    <span className="left">55%</span>
                                    <span className="right">55$ / 100$</span>
                                </Col>
                                <Col s={12}>
                                    <ProgressBar progress={55} />
                                </Col>
                                <Col s={12}>
                                    <span className="left"><Icon left>face</Icon> 9</span>
                                    <span className="right">Expires: 2020-02-15</span>
                                </Col>
                            </Row>
                                
                            {/* We are South Sudanese trying our best to feed our hungry neighbors during these difficult times. South Sudan has experienced 7,000% inflation in the last three years, devastating the lives of its people. Thanks to generous donations from around the world, we've been able to help out. */}
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non tincidunt quam. Cras maximus massa odio, non interdum nunc dictum non. Fusce mollis quis tortor a tincidunt. Donec quis tincidunt felis, condimentum interdum eros. Curabitur semper id felis condimentum ullamcorper. Cras id dolor pulvinar leo facilisis semper. 
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Home;