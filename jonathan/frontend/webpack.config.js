const webpack = require('webpack');

module.exports =
{
	mode: 'production',

	// Add your application's scripts below
	entry: [ './www/js/source.js' ],
	output: 
	{
		filename: './www/js/application.js'
	},
	plugins:
	[
	    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
	]
}
