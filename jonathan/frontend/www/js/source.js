// Load the moment library to better manage time.
const moment = require('moment');

// Load the locales we will use.
import 'moment/locale/en-gb.js';
import 'moment/locale/zh-cn.js';

// Load the marked library to parse markdown text,
const marked = require('marked');

// Load and initialize the DOMPurify library to ensure safety for parsed markdown.
const createDOMPurify = require('dompurify');
const DOMPurify = createDOMPurify(window);

// Load the celebratory confetti library.
const confetti = require('canvas-confetti').default;

//
const commitmentsPerTransaction = 650;
const SATS_PER_BCH = 100000000;

const CAMPAIGN_ID = (window.location.hash.slice(1) || 1);

// assume a static miner fee for now.
const STATIC_FEE = 5000;

//
class flipstarter
{
	constructor()
	{
		// Get the main language from the browser.
		const language = window.navigator.language.slice(0, 2);

		// Load the initial translation files in the background.
		this.loadTranslation(language);

		// Once the page is loaded, initialize flipstarter.
		window.addEventListener("load", this.initialize.bind(this));
	}

	async initialize()
	{
		// Attach event handlers.
		document.getElementById('donationSlider').addEventListener("input", this.updateContributionInput.bind(this));
		document.getElementById('donateButton').addEventListener("click", this.toggleDonationSection.bind(this));

		document.getElementById('template').addEventListener("click", this.copyTemplate.bind(this));
		document.getElementById('copyTemplateButton').addEventListener("click", this.copyTemplate.bind(this));
		document.getElementById('commitTransaction').addEventListener("click", this.parseCommitment.bind(this));

		//
		document.getElementById('contributionName').addEventListener("change", this.updateTemplate.bind(this));
		document.getElementById('contributionName').addEventListener("keyup", this.updateTemplate.bind(this));
		document.getElementById('contributionComment').addEventListener("change", this.updateTemplate.bind(this));
		document.getElementById('contributionComment').addEventListener("keyup", this.updateTemplate.bind(this));

		//
		document.getElementById('translateEnglish').addEventListener("click", this.updateTranslation.bind(this, 'en', 'English'));
		document.getElementById('translateChinese').addEventListener("click", this.updateTranslation.bind(this, 'zh', 'Chinese'));

		// Get the main language from the browser.
		const language = window.navigator.language.slice(0, 2);

		// Wait for translations to finish loading..
		await this.translationLoadingPromise;

		// Apply website translation (or load website content).
		this.applyTranslation(language);

		// Fetch the campaign information from the backend.
		let response = await fetch(`https://backend.flipstarter.cash/campaign/${CAMPAIGN_ID}`);
		let fundraiser = await response.json();

		this.campaign = fundraiser.campaign;
		this.campaign.recipients = fundraiser.recipients;
		this.campaign.contributions = {};

		// If this campaign has already been fullfilled..
		if(this.campaign.fullfillment_timestamp > 0)
		{
			// Mark the campaign as fullfilled which prevents form entry.
			this.updateStatus('statusFullfilled', this.translation['statusFullfilled']);
		}
		else
		{
			document.getElementById('donateForm').className = 'col s12 m12 l12';
		}

		//
		this.updateRecipientCount(this.campaign.recipients.length);
		this.updateCampaignProgressCounter();

		// Add each recipient to the fundraiser.
		for(const recipientIndex in fundraiser.recipients)
		{
			const recipientAmount = Number(fundraiser.recipients[recipientIndex].recipient_satoshis / SATS_PER_BCH).toLocaleString();
			const recipientFiat = Number(fundraiser.recipients[recipientIndex].recipient_satoshis / SATS_PER_BCH * this.currencyValue).toFixed(2);
			const recipientName = fundraiser.recipients[recipientIndex].user_alias;
			const recipientAddress = fundraiser.recipients[recipientIndex].user_address;
			const recipientURL = fundraiser.recipients[recipientIndex].user_url;

			document.getElementById('recipientList').innerHTML +=
			`<li>
				<a href='${recipientURL}'>
					<img src='${fundraiser.recipients[recipientIndex].user_image}' alt='${recipientName}' />
					<span>
						<b>${recipientName}</b>
						<i>${recipientAmount} BCH</i>
					</span>
				</a>
			</li>`;
		}

		// Update the input to reflect the current langauge.
		document.getElementById('donationSlider').dispatchEvent(new Event('input'));

		// Initialize the language selector.
		{
			// Fetch the DOM element.
			const languageSelector = document.getElementById('languageSelector');

			// Add mouse over and mouse out events.
			languageSelector.addEventListener("mouseover", function() { languageSelector.className = 'fixed-action-btn direction-bottom active' });
			languageSelector.addEventListener("mouseout", function() { languageSelector.className = 'fixed-action-btn direction-bottom' });
		}

		//
		const activateInputField = function(event)
		{
			const targetName = event.target.id;
			const label = document.querySelector(`label[for=${targetName}]`);

			if((document.activeElement === event.target) || (event.target.value > ''))
			{
				label.className = 'active';
			}
			else
			{
				label.className = '';
			}
		}

		//
		const contributionNameInput = document.getElementById('contributionName');
		const contributionCommentInput = document.getElementById('contributionComment');

		//
		contributionNameInput.addEventListener("focus", activateInputField);
		contributionNameInput.addEventListener("blur", activateInputField);
		contributionCommentInput.addEventListener("focus", activateInputField);
		contributionCommentInput.addEventListener("blur", activateInputField);

		// TEMP.
		//this.buildTranslationFile();

		const parseContributionEvents = function(event)
		{
			const eventData = JSON.parse(event.data);

			// Special case: fullfillment.
			if(eventData.fullfillment_transaction)
			{
				// Trigger celebration.
				celebration(0.11);

				// Update form to prevent further entry.
				this.updateStatus('statusFullfilled', this.translation['statusFullfilled']);
			}
			else
			{
				// If the data refers to the current campaign...
				if(eventData.campaign_id == CAMPAIGN_ID)
				{
					// .. and the data has been revoked before fullfillment..
					if(eventData.revocation_id && (!this.campaign.fullfillment_timestamp || (eventData.revocation_timestamp < this.campaign.fullfillment_timestamp)))
					{
						// .. remove it if we know it from earlier
						if(typeof(this.campaign.contributions[eventData.contribution_id]) !== 'undefined')
						{
							// Delete it locally.
							delete this.campaign.contributions[eventData.contribution_id];
						}
					}
					else
					{
						// .. store the contribution locally.
						this.campaign.contributions[eventData.contribution_id] = eventData;
					}

					// .. update the contribution list.
					this.updateContributionList();

					// .. update the progress bar and contribution amount
					document.getElementById('campaignProgressBar').style.width = (100 * this.countCommittedSatoshis(this.campaign.contributions) / this.countRequestedSatoshis(this.campaign.recipients)).toFixed(2) + "%";
					document.getElementById('compaignContributionAmount').innerHTML = DOMPurify.sanitize((this.countCommittedSatoshis(this.campaign.contributions) / SATS_PER_BCH).toFixed(2));
				}
			}
		};

		//
		this.eventSource = new EventSource("https://backend.flipstarter.cash/events/");
		this.eventSource.addEventListener('message', parseContributionEvents.bind(this));

		//
		setInterval(this.updateExpiration.bind(this), 500);
	}

	countCommittedSatoshis(contributions)
	{
		let committedSatoshis = 0;

		for(const contributionIndex in contributions)
		{
			if(typeof contributions[contributionIndex].satoshis !== 'undefined')
			{
				committedSatoshis += contributions[contributionIndex].satoshis;
			}
		}

		return committedSatoshis;
	}

	countRequestedSatoshis(recipients)
	{
		let requestedSatoshis = 0;

		for(const recipientIndex in recipients)
		{
			requestedSatoshis += Number(recipients[recipientIndex].recipient_satoshis);
		}

		return requestedSatoshis;
	}

	async updateContributionList()
	{
		const contributionListElement = document.getElementById('contributionList');

		// Empty the contribution list.
		contributionListElement.innerHTML = '';

		// Update the contribution counter.
		document.getElementById('campaignContributorCount').innerHTML = Object.keys(this.campaign.contributions).length;

		if(Object.keys(this.campaign.contributions).length == 0)
		{
			// Get the empty message template node.
			const template = document.getElementById('emptyContributionMessage').content.firstElementChild;

			// Import a copy of the template.
			const contributionMessage = document.importNode(template, true);

			// Add the copy to the contribution list.
			contributionListElement.appendChild(contributionMessage);
		}
		else
		{
			const contributionArray = Object.values(this.campaign.contributions);
			const sortedContributions = contributionArray.sort((a, b) => Number(b.satoshis) - Number(a.satoshis));
			for(const contributionIndex in sortedContributions)
			{
				//
				const contribution = sortedContributions[contributionIndex];

				this.addContributionToList(contribution.user_alias, contribution.contribution_comment, contribution.satoshis, contribution.satoshis / this.countRequestedSatoshis(this.campaign.recipients));
			}
		}
	}

	async buildTranslationFile()
	{
		//
		let strings = [];

		// Fetch all elements to be translated.
		const elements = document.body.querySelectorAll('*[data-label]');

		// For each element..
		for(const index in elements)
		{
			if(typeof elements[index] == 'object')
			{
				// Get the translation string key.
				strings.push({ index: elements[index].getAttribute('data-label') });
			}
		}

		console.log(JSON.stringify(strings));
	}

	async updateTranslation(locale = 'en')
	{
		// Load the new translation.
		this.loadTranslation(locale);

		// Wait for translations to finish loading..
		await this.translationLoadingPromise;

		// Update the rendered translation.
		this.applyTranslation(locale);

		// Update the input to reflect the current langauge.
		document.getElementById('donationSlider').dispatchEvent(new Event('input'));
	}

	async loadTranslation(locale = 'en')
	{
		// Set default language.
		let languageCode = 'en';
		let languageName = 'English';
		let languageCurrencyCode = 'USD';

		// Make a list of supported translations.
		const languages =
		{
			en: 'English',
			zh: 'Chinese'
		};

		// Make a list of moment locales to use for each language.
		const momentLocales =
		{
			en: 'en-gb',
			zh: 'zh-cn'
		}

		// Make a list of currencies to use for each language.
		const currencies =
		{
			en: 'USD',
			zh: 'CNY'
		};

		// If the requested language has a translation..
		if(typeof languages[locale] !== 'undefined')
		{
			// Overwrite the default language.
			languageCode = locale;
			languageName = languages[locale];
			languageCurrencyCode = currencies[locale];
		}

		// Initiate all requests in parallell.
		this.translationContentPromises =
		{
			interfaceResponse: fetch(`https://backend.flipstarter.cash/ui/${languageCode}/interface.json`),
			introResponse: fetch(`https://backend.flipstarter.cash/campaigns/${CAMPAIGN_ID}/${languageCode}/abstract.md`),
			detailResponse: fetch(`https://backend.flipstarter.cash/campaigns/${CAMPAIGN_ID}/${languageCode}/proposal.md`),
			currencyResponse: fetch(`https://bitpay.com/api/rates/BCH/${languageCurrencyCode}`)
		}

		// Wait for all requests to complete..
		this.translationLoadingPromise = Promise.all(Object.values(this.translationContentPromises));
	}

	async applyTranslation(locale = 'en')
	{
		// Set default language.
		let languageCode = 'en';
		let languageName = 'English';
		let languageCurrencyCode = 'USD';

		// Make a list of supported translations.
		const languages =
		{
			en: 'English',
			zh: 'Chinese'
		};

		// Make a list of moment locales to use for each language.
		const momentLocales =
		{
			en: 'en-gb',
			zh: 'zh-cn'
		}

		// Make a list of currencies to use for each language.
		const currencies =
		{
			en: 'USD',
			zh: 'CNY'
		};

		// If the requested language has a translation..
		if(typeof languages[locale] !== 'undefined')
		{
			// Overwrite the default language.
			languageCode = locale;
			languageName = languages[locale];
			languageCurrencyCode = currencies[locale];
		}

		// Update the HTML language reference.
		document.getElementsByTagName('html')[0].setAttribute('lang', languageCode);
		document.getElementById('currentLanguage').innerHTML = languageName;

		// Get the currency exchange information.
		let currencyData = await (await this.translationContentPromises.currencyResponse).json();

		// Store the current code and exchange rate.
		this.currencyCode = languageCurrencyCode;
		this.currencyValue = currencyData.rate;

		// Parse the campaign translations.
		const campaignIntro = await (await this.translationContentPromises.introResponse).text();
		const campaignDetail = await (await this.translationContentPromises.detailResponse).text();

		// Print out the campaign texts.
		document.getElementById('campaignAbstract').innerHTML = DOMPurify.sanitize(marked(campaignIntro));
		document.getElementById('campaignDetails').innerHTML = DOMPurify.sanitize(marked(campaignDetail));

		// Parse the interface translation.
		this.translation = await (await this.translationContentPromises.interfaceResponse).json();

		// Fetch all elements to be translated.
		const elements = document.body.querySelectorAll('*[data-label]');

		// For each element..
		for(const index in elements)
		{
			if(typeof elements[index] == 'object')
			{
				// Get the translation string key.
				const key = elements[index].getAttribute('data-label');

				// TODO: Look up the translation from a translation table.
				const value = this.translation[key];

				// Print out the translated value.
				elements[index].innerHTML = value;
			}
		}

		// Fetch all templates to be translated.
		const templates = document.body.querySelectorAll('template');

		// For each template..
		for(const templateIndex in templates)
		{
			if(typeof templates[templateIndex].content !== 'undefined')
			{
				// Fetch all elements to be translated.
				const templateElements = templates[templateIndex].content.querySelectorAll('*[data-label]');

				for(const index in templateElements)
				{
					if(typeof templateElements[index] == 'object')
					{
						// Get the translation string key.
						const key = templateElements[index].getAttribute('data-label');

						// TODO: Look up the translation from a translation table.
						const value = this.translation[key];

						// Print out the translated value.
						templateElements[index].innerHTML = value;
					}
				}
			}
		}

		// Change moment to use the new locale.
		moment.locale(momentLocales[languageCode]);
	}

	async copyTemplate()
	{
		// Locate the template input elements.
		const templateTextArea = document.getElementById("template");
		const templateButton = document.getElementById("copyTemplateButton");

		// Select the template text.
		templateTextArea.select();
		templateTextArea.setSelectionRange(0, 99999);

		// Copy the selection to the clipboard.
		document.execCommand("copy");

		// Deselect the text.
		templateTextArea.setSelectionRange(0, 0);

		// Notify user that the template has been copied by changing the button appearance..
		templateButton.innerHTML = 'Done';
		templateButton.disabled = 'disabled';
	}

	async toggleDonationSection(visibility = null)
	{
		const donationSection = document.getElementById('donateSection');

		if(visibility != false && donationSection.className != 'visible col s12 m12')
		{
			donationSection.className = 'visible col s12 m12';
		}
		else
		{
			donationSection.className = 'hidden col s12 m12';
		}
	}

	async updateCampaignProgressCounter()
	{
		document.getElementById('campaignRequestAmount').innerHTML = DOMPurify.sanitize((this.countRequestedSatoshis(this.campaign.recipients) / SATS_PER_BCH).toFixed(2));
	}

	async updateExpiration()
	{
		document.getElementById('campaignExpiration').innerHTML = moment().to(moment.unix(this.campaign.expires));
	}

	async updateRecipientCount(recipientCount)
	{
		document.getElementById('campaignRecipientCount').innerHTML = recipientCount;
	}

	async updateTemplate()
	{
		// Locate the template input elements.
		const templateTextArea = document.getElementById("template");
		const templateButton = document.getElementById("copyTemplateButton");

		//
		templateButton.innerHTML = 'Copy template';
		templateButton.disabled = null;

		// Get the number of satoshis the user wants to donate.
		const satoshis = document.getElementById('donationAmount').getAttribute('data-satoshis');

		// If the user wants to donate some satoshis..
		if(satoshis)
		{
			// Assemble the request object.
			let requestObject =
			{
				outputs: [],
				data:
				{
					alias: document.getElementById("contributionName").value,
					comment: document.getElementById("contributionComment").value
				},
				donation:
				{
					amount: Number(satoshis)
				},
				expires: this.campaign.expires
			};

			// For each recipient..
			for(const recipientIndex in this.campaign.recipients)
			{
				const outputValue = this.campaign.recipients[recipientIndex].recipient_satoshis;
				const outputAddress = this.campaign.recipients[recipientIndex].user_address;

				// Add the recipients outputs to the request.
				requestObject.outputs.push({ value: outputValue, address: outputAddress });
			}

			// Assemble an assurance request template.
			const templateString = btoa(JSON.stringify(requestObject));

			// Update the website template string.
			templateTextArea.innerHTML = templateString;
		}
		else
		{
			// Update the website template string.
			templateTextArea.innerHTML = '';
		}
	}

	async updateContributionInput(event)
	{
		let donationAmount;
		const requestedSatoshis = this.countRequestedSatoshis(this.campaign.recipients);
		const committedSatoshis = this.countCommittedSatoshis(this.campaign.contributions);

		if(event.target.value == 0)
		{
			// Hide donation section.
			this.toggleDonationSection(false);

			// Disable action button.
			document.getElementById('donateButton').disabled = true;

			// Reset metadata.
			document.getElementById('contributionName').value = '';
			document.getElementById('contributionComment').value = '';

			// Set amount to 0.
			donationAmount = 0;
		}
		else
		{
			document.getElementById('donateButton').disabled = false;
			donationAmount = Math.ceil((STATIC_FEE + requestedSatoshis - committedSatoshis) * await this.inputPercentModifier(event.target.value));
		}

		if(event.target.value == 100)
		{
			document.getElementById('donateText').innerHTML = this.translation['fullfillText'];
		}
		else
		{
			document.getElementById('donateText').innerHTML = this.translation['donateText'];
		}

		document.getElementById('campaignContributionBar').style.left = (100 * (committedSatoshis / requestedSatoshis)).toFixed(2) + '%';
		document.getElementById('campaignContributionBar').style.width = (100 * await this.inputPercentModifier(event.target.value) * (1 - (committedSatoshis / requestedSatoshis))).toFixed(2) + "%";
		document.getElementById('donationAmount').innerHTML = (donationAmount / SATS_PER_BCH).toLocaleString() + ' BCH (' + (this.currencyValue * (donationAmount / SATS_PER_BCH)).toFixed(2) + ' ' + this.currencyCode + ')';

		//
		document.getElementById('donationAmount').setAttribute('data-satoshis', donationAmount);

		// Update the template text.
		this.updateTemplate();
	}

	async inputPercentModifier(inputPercent)
	{
		// Calculate how many % of the total fundraiser the smallest acceptable contribution is at the moment.
		const remainingValue = STATIC_FEE + (this.countRequestedSatoshis(this.campaign.recipients) - this.countCommittedSatoshis(this.campaign.contributions));

		const currentTransactionSize = 42; //this.contract.assembleTransaction().byteLength;

		const minPercent = 0 + (((remainingValue / (commitmentsPerTransaction - this.campaign.recipients.length)) + (546 / SATS_PER_BCH)) / remainingValue);
		const maxPercent = 1 - (((currentTransactionSize + 1650 + 49) * 1.0) / (remainingValue * SATS_PER_BCH));

		// ...
		const minValue = Math.log(minPercent * 100);
		const maxValue = Math.log(maxPercent * 100);

		// Return a percentage number on a non-linear scale with higher resolution in the lower boundaries.
		return (Math.exp(minValue + inputPercent * (maxValue - minValue) / 100) / 100);
	}

	/**
	 *
	 */
	async parseCommitment()
	{
		//
		const base64text = document.getElementById('commitment').value;

		//
		const commitmentObject = JSON.parse(atob(base64text));

		//
		const contributionName = document.getElementById('contributionName').value;
		const contributionComment = document.getElementById('contributionComment').value;

		// Validate that the commitment data matches the expectations.
		// Check that the contribution uses the correct structure.
		if(typeof commitmentObject.inputs == 'undefined')
		{
			throw(`Parsed commitment is not properly structured.`);
		}

		// Check that the contribution uses the same name.
		if(commitmentObject.data.alias != contributionName)
		{
			throw(`Parsed commitments alias '${commitmentObject.data.alias}' does not match the contributors name '${contributionName}'.`);
		}

		// Check that the contribution uses the same comment.
		if(commitmentObject.data.comment != contributionComment)
		{
			throw(`Parsed commitments alias '${commitmentObject.data.comment}' does not match the contributors comment '${contributionComment}'.`);
		}

		// TODO: Validate the data signature, if present..
		if(typeof commitmentObject.data_signature != 'undefined')
		{

		}

		// Disable the commit button to prevent confusion.
		document.getElementById('commitTransaction').disabled = true;

		// NOTE: It is not possible to verify the amount without access to the UTXO database.
		// TODO: Pass through the intended amount so that verification can happen on backend.

		//
		const submissionOptions =
		{
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			credentials: 'same-origin',
			headers:
			{
				'Content-Type': 'application/json'

			},
			body: JSON.stringify(commitmentObject)
		}

		// Submit the commitment to the backend.
		const submissionPromise = fetch('https://backend.flipstarter.cash/submit/' + CAMPAIGN_ID, submissionOptions);
		const submissionStatus = await submissionPromise;

		//
		const event = new InputEvent('input');

		// Clear out the pasted commitment.
		document.getElementById('commitment').value = '';

		// Set the donation slider to 0.
		document.getElementById('donationSlider').value = 0;

		// Update the donation input form.
		document.getElementById('donationSlider').dispatchEvent(event);

		// Re-enable the commit button to allow new contributions..
		document.getElementById('commitTransaction').disabled = false;

		// Update form to indicate success and prevent further entry.
		this.updateStatus('statusContribution', this.translation['statusContribution']);
	}

	updateStatus(label, content)
	{
		const donateStatus = document.getElementById('donateStatus');
		const donateForm =  document.getElementById('donateForm');
		const donateSection = document.getElementById('donateSection');

		// Hide form and section.
		donateForm.className = 'col s12 m12 l12 hidden';
		donateSection.className = 'col s12 m12 l12 hidden';

		// Add status content.
		donateStatus.setAttribute('data-label', label);
		donateStatus.innerHTML = content;

		// Show status.
		donateStatus.className = 'col s12 m12 l12';
	}

	addContributionToList(alias, comment, amount, percent)
	{
		// Get the contribution list.
		const contributionList = document.getElementById('contributionList');

		// Get the template node.
		const template = document.getElementById('contributionTemplate').content.firstElementChild;

		// Import a copy of the template.
		const contributionEntry = document.importNode(template, true);

		// Calculate water level and randomize animation delay.
		const backgroundMin = 0.1;
		const backgroundMax = 3.5;
		const backgroundPosition = (backgroundMin + (backgroundMax * (1 - percent))).toFixed(2);
		const animationLength = 15;
		const animationDelay = (Math.random() * animationLength).toFixed(2);
		const contributionAmount = (amount / SATS_PER_BCH).toFixed(2);

		// Update the data on the copy.
		contributionEntry.querySelector('.contributionDisplay').style.backgroundPosition = `0 ${backgroundPosition}rem`;
		contributionEntry.querySelector('.contributionDisplay').style.animationDelay = `-${animationDelay}s`;
		contributionEntry.querySelector('.contributionPercent').innerHTML = (percent * 100).toFixed(0) + '%';
		contributionEntry.querySelector('.contributionAlias').innerHTML = alias;
		contributionEntry.querySelector('.contributionComment').innerHTML = DOMPurify.sanitize(comment, {ALLOWED_TAGS: []});
		contributionEntry.querySelector('.contributionAmount').innerHTML = `${contributionAmount} BCH`;

		// Hide the comment if not existing.
		if(comment == '')
		{
			contributionEntry.querySelector('.contributionComment').style.display = 'none';
		}

		// Mark username as anonymous if not existing.
		if(alias == '')
		{
			contributionEntry.querySelector('.contributionAlias').style.opacity = 0.37;
			contributionEntry.querySelector('.contributionAlias').innerHTML = 'Anonymous';
		}

		// Add the copy to the contribution list.
		contributionList.appendChild(contributionEntry);
	}
}

// Start the application.
window.flipstarter = new flipstarter();



// Function that can be used to cause celebratory effects.
const celebration = function(volume)
{
	// Let the confetti burst in like fireworks!
	let fireworks = function()
	{
		// Left side of the screen.
		const leftConfetti =
		{
			particleCount: 50,
			angle: 60,
			spread: 90,
			origin: { x: 0 }
		};

		// Right side of the screen.
		const rightConfetti =
		{
			particleCount: 50,
			angle: 120,
			spread: 90,
			origin: { x: 1 }
		}

		// Trigger the confetti.
		confetti(leftConfetti);
		confetti(rightConfetti);
	}

	// Adjust volume to prevent heartattacks.
	document.getElementById('applause').volume = volume;

	// Play the sound effect.
	document.getElementById('applause').play();

	// Burst multiple times with some delay.
	window.setTimeout(fireworks, 100);
	window.setTimeout(fireworks, 200);
	window.setTimeout(fireworks, 400);
	window.setTimeout(fireworks, 500);
	window.setTimeout(fireworks, 700);
}
